import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  title = 'default title';
  subject = new Subject<string>();

  constructor() {}

  logMe(msg: string) {
    console.log(msg);
  }

  setTitle(title:string) {
    console.log('set title');
    this.title = title;
    this.subject.next(this.title);
  }

  titleChange(): Observable<string> {
    return this.subject.asObservable();
  }

  // Observable example
  wathing() : Observable<any> {
    let title = new Observable((observer) => {
      setInterval(() => {
        observer.next(); // trigger the changes
      }, 5000); // run every 5 seconds
    });
    // see also setTimeOut() - run one time only
    return title;
  }
}
