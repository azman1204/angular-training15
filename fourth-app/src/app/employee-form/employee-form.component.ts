import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '../employee.service';
import { Employee } from '../models/employee.model';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {
  emp: Employee = new Employee();
  mode = 'update';

  constructor(private router: Router, private route:ActivatedRoute, private empService: EmployeeService) {
    console.log(this.route.snapshot.params['empid']);
  }

  ngOnInit(): void {
    if (this.route.snapshot.params['empid']) {
      // this for edit only
      let empid = this.route.snapshot.params['empid'];
      this.emp = this.empService.find(empid);
    } else {
      this.mode = 'create';
    }
  }

  doSubmit() {
    if (this.mode === 'update') {
      this.empService.update(this.emp);
    } else {
      this.empService.insert(this.emp);
    }
    this.router.navigate(['employee']);
  }
}
