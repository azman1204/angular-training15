import { Component, OnInit } from '@angular/core';
import { BookService } from './book.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'fourth-app';

  // this is dependency injection (DI)
  constructor(public bookService:BookService) {
    bookService.logMe('Hello service');
    bookService.title = 'Lord of The Ring';
  }

  // subscribe to Observable
  ngOnInit(): void {
    this.bookService.wathing().subscribe(() => {
      console.log('something changed....');
    });
  }

  doTitle(event: Event) {
    let title = (event.target as HTMLInputElement).value;
    console.log(title);
    this.bookService.setTitle(title);
  }
}
