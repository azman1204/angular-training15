import { Injectable } from '@angular/core';
import { Employee } from './models/employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  employees: Employee[];

  constructor() {
    this.employees = [];
    let emp = new Employee();
    emp.email = 'azman@gmail.com';
    emp.empid = 1;
    emp.name = 'azman';
    emp.post = 'Programmer';
    this.employees.push(emp);

    let emp2 = new Employee();
    emp2.email = 'abu@gmail.com';
    emp2.empid = 2;
    emp2.name = 'Abu Bakar';
    emp2.post = 'System Analyst';
    this.employees.push(emp2);
  }

  find(empid: number) {
    let arr = this.employees.filter((emp) => {
      return emp.empid == empid;
    });
    return arr[0];
  }

  update(emp: Employee) {
    this.employees = this.employees.filter((e) => {
      if (e.empid == emp.empid) {
        e.name = emp.name;
        e.post = emp.post;
        e.email = emp.email;
        return true;
      } else {
        return true;
      }
    });
  }

  delete(empid: number) {
    this.employees = this.employees.filter((emp) => {
      if (emp.empid == empid) {
        return false;
      } else {
        return true;
      }
    })
  }

  insert(emp: Employee) {
    this.employees.push(emp);
  }
}
