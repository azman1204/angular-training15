import { Component } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Employee } from '../models/employee.model';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent {
  employees: Employee[];

  constructor(private empService: EmployeeService) {
    this.employees = empService.employees;
  }

  doDelete(empid: number) {
    if (confirm('Are you sure to delete')) {
      this.empService.delete(empid);
      this.employees = this.empService.employees;
    }
  }
}
