import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { BookListComponent } from './book-list/book-list.component';
import { PostComponent } from './post/post.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeFormComponent } from './employee-form/employee-form.component';

const routes: Routes = [
  {path: 'book', component: BookListComponent}, // localhost:4200/book
  {path: 'post', component: PostComponent}, // localhost:4200/post
  {path: 'employee', component: EmployeeComponent}, // localhost:4200/post
  {path: 'employee-new', component: EmployeeFormComponent}, // localhost:4200/post
  {path: 'employee-form/:empid', component: EmployeeFormComponent}, // localhost:4200/post
  //{path: '**', component: NotFoundComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    BookListComponent,
    PostComponent,
    EmployeeComponent,
    EmployeeFormComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
