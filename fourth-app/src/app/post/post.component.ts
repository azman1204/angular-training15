import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  posts: Post[];
  posts_ori : Post[];
  title = '';

  constructor(private http:HttpClient) {
    let obj = {userId: 0, id: 0, title:'', body:''};
    this.posts = [];
    this.posts.push(obj);
    this.posts_ori = this.posts;
  }

  ngOnInit(): void {
    this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts').subscribe((data) => {
      console.log(data);
      this.posts = data;
      this.posts_ori = this.posts;
    });
  }

  doSearch() {
    console.log(this.title);
    this.posts = this.posts_ori;
    this.posts = this.posts.filter((post) => {
      if (post.title.includes(this.title)) {
        return true;
      } else {
        return false;
      }
    });
  }
}

interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}
