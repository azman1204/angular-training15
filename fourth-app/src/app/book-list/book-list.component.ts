import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {
  title = '';

  constructor(public bookService: BookService) {
    this.title = bookService.title;
  }

  ngOnInit(): void {
    this.bookService.titleChange().subscribe(() => {
      console.log('in book-list comnponent .. title changed');
      this.title = this.bookService.title;
    });
  }

  doRefresh() {
    this.title = this.bookService.title;
  }
}
