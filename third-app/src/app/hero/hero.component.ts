import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {
  mytitle: string;
  show = true;

  constructor() {
    // is used to initailize the property
    console.log('im in constructor');
    this.mytitle = 'Hello World';
  }

  ngOnInit(): void {
    // others initialization. access API, get some default data and show
    console.log('im in ngOnInit');
  }

  onDestroy() {
    console.log('destroy');
  }

  onAlert(title:string) {
    alert(title);
    this.show = false;
  }
}
