import { Component } from '@angular/core';

@Component({
  selector: 'app-show-hide',
  templateUrl: './show-hide.component.html',
  styleUrls: ['./show-hide.component.css']
})
export class ShowHideComponent {
  show = true;
  label = 'Hide';
  names = ['Azman',' Atikah', 'Haslina', 'Moi', 'Sarvendan'];
  name = 'xxx';

  doClick() {
    this.show = !this.show;
    if (this.show) {
      this.label = 'Hide';
    } else {
      this.label = 'Show';
    }
  }
}
