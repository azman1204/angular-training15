import { Component } from '@angular/core';

@Component({
  selector: 'app-assignment4',
  templateUrl: './assignment4.component.html',
  styleUrls: ['./assignment4.component.css']
})
export class Assignment4Component {
  no = 1;
  show = false;
  // logs:string[] = [];
  logs:number[] = [];
  interest_rate = 0.027;
  rm = 500;
  today = new Date();

  doClick() {
    this.show = !this.show;
    //this.logs.push(Date().toString());
    this.logs.push(this.no++);
  }

  getColor() {
    return 'gold';
  }

  getClass() {
    return {
      biru: true,
      test: true
    }
  }
}
