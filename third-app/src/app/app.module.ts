import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeroComponent } from './hero/hero.component';
import { AlertComponent } from './alert/alert.component';
import { ShowHideComponent } from './demo/show-hide/show-hide.component';
import { Assignment4Component } from './demo/assignment4/assignment4.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroComponent,
    AlertComponent,
    ShowHideComponent,
    Assignment4Component
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
