import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, OnDestroy {
  ngOnInit(): void {
    console.log('initializing..');
  }
  // this is how we expose a property to other component
  @Input() title = 'Default Title';
  @Input() mode = 'success';
  // this is custom event - child to parent communication
  @Output() ok = new EventEmitter();
  mycolor = '#eee';

  onOk() {
    this.ok.emit('You click ok');
  }

  ngOnDestroy() {
    // cleaningup resource
    console.log('destroy');
  }
}
