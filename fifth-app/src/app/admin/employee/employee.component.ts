import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  employees: Employee[] = [];
  employees_ori: Employee[] = [];
  name = '';

  constructor(private empService: EmployeeService) {

  }

  doSearch() {
    console.log('searching', this.name);
    this.employees = this.employees_ori;
    this.employees = this.employees.filter((emp) => {
      return emp.name.toLowerCase().includes(this.name.toLowerCase());
    });
  }

  ngOnInit(): void {
    this.listEmployee();
  }

  listEmployee() {
    this.empService.findAll().subscribe((data: Employee[]) => {
      this.employees = data;
      this.employees_ori = this.employees;
    });
  }

  onDelete(id: number) {
    //alert(id);
    if (confirm('Are you sure ?')) {
      this.empService.delete(id);
      this.listEmployee();
    }
  }
}
