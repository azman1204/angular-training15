import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeFormComponent } from './employee-form/employee-form.component';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from '../auth.guard';

const routes: Routes = [
  { path: 'user-list', component: UserListComponent, canActivate: [AuthGuard]},
  { path: 'employee-list', component: EmployeeComponent, canActivate: [AuthGuard]},
  { path: 'employee-new', component: EmployeeFormComponent},
  { path: 'employee-edit/:id', component: EmployeeFormComponent},
];

@NgModule({
  declarations: [
    EmployeeComponent,
    EmployeeFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ]
})
export class AdminModule { }
