import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {
  emp = new Employee();
  mode = 'create';

  constructor(private empService: EmployeeService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    if (this.route.snapshot.params['id']) {
      this.mode = 'edit';
      let id = this.route.snapshot.params['id'];
      this.empService.findOne(id).subscribe((data) => {
        this.emp = data;
      });
    }
  }

  doSubmit() {
    if (this.mode === 'create')
      this.empService.insert(this.emp);
    else
      this.empService.update(this.emp.id, this.emp);

    // redirect to previous page
    this.router.navigate(['employee-list']);
  }
}
