import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { MaterialComponent } from './material/material.component';
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'material', component: MaterialComponent},
];

@NgModule({
  declarations: [
    LoginComponent,
    MaterialComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatTableModule,
    RouterModule.forRoot(routes)
  ]
})
export class MainModule { }
