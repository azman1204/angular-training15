import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  userid = '';
  password = '';

  constructor(private login:LoginService, private router: Router) {

  }

  doLogin() {
    let ok = this.login.auth(this.userid, this.password);
    if (ok) {
      // redirect to home page ...
      this.router.navigate(['employee-list']);
    } else {
      alert('wrong userid and password');
    }
  }
}
