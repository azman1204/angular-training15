import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn = false;
  roles = '';

  constructor() { }

  auth(userid: string, password: string) : boolean {
    if (userid === 'john' && password === '1234') {
      this.isLoggedIn = true;
      this.roles = 'admin';
    }

    if (userid === 'jane' && password === '1234') {
      this.isLoggedIn = true;
      this.roles = 'auditor';
    }

    return this.isLoggedIn;
  }

  logmeout() {
    this.isLoggedIn = false;
  }
}
