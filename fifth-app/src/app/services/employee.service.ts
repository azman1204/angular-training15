import { Injectable } from '@angular/core';
import { Employee } from '../models/employee.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  url = 'http://localhost:3000/employee';

  constructor(private http: HttpClient) { }

  findOne(id:number) : Observable<Employee> {
    return this.http.get<Employee>(this.url + "/" + id);
  }

  // retrieve all employees
  findAll() : Observable<Employee[]> {
    return this.http.get<Employee[]>(this.url);
  }

  insert(emp: Employee) {
    this.http.post(this.url, emp).subscribe((data) => {
      console.log(data);
    });
  }

  update(id:number, emp:Employee) {
    this.http.put(this.url + "/" + id, emp).subscribe((data) => {
      console.log(data);
    })
  }

  delete(id:number) {
    this.http.delete(this.url + "/" + id)
    .subscribe((data) => {
      console.log(data);
    });
  }
}
