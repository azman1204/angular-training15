import { Component } from '@angular/core';
import { LoginService } from './services/login.service';
import { Router, Routes } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'fifth-app';

  constructor(private loginService:LoginService, private route:Router) {

  }

  logmeout() {
    this.loginService.logmeout();
    this.route.navigate(['login']);
  }
}
