import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent {
  lang = 'en';

  constructor(public translate: TranslateService) {
    translate.use(this.lang);
  }

  doLang() {
    this.translate.use(this.lang);
  }
}
