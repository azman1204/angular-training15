import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('sum two 2 + 3 = 5', () => {
    expect(component.sum(2 ,3)).toBe(5);
  });

  it('should say hello azman', () => {
    const html = fixture.nativeElement as HTMLElement;
    expect(html.querySelector('#sample').textContent).toContain('Hello azman');
  });
});
