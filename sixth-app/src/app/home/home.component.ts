import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  name = 'john smith';

  sum(num1: number, num2: number) : number {
    return num1 + num2;
  }
}
