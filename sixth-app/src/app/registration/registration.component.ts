import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {
  myform = new FormGroup({
    name:  new FormControl('', Validators.required),
    post:  new FormControl(''),
    email: new FormControl(''),
  });

  doSave() {
    //console.log(this.myform.value);
    let post = this.myform.get('post');
    console.log(post);
    console.log(this.myform.get('name'));
  }

  // method which behave like a property. getName(), name
  get name() {
    return this.myform.get('name');
  }

  get post() {
    return this.myform.get('post');
  }

  get email() {
    return this.myform.get('email');
  }
}
