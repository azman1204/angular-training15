import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {
  title = 'Superman';
  url = 'https://google.com';
  yahoo = 'www.yahoo.com';
  myclass = {
    star: true,
    active: true
  };

  constructor() { }

  ngOnInit(): void {

  }

}
