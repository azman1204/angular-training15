import { Component } from '@angular/core';

@Component ({
  selector: 'app-hello',
  template: `
  <h1>Hello World</h1>
  <p>world</p>
  `,
  styles: ['h1 { color:red }', 'p { color : green }']
})
export class HelloComponent {

}
