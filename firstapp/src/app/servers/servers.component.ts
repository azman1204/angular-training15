import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  // data binding
  text = 'default';
  text2 = 'default';

  constructor() { }

  ngOnInit(): void {
  }

  onClick() {
    alert('You click me ');
  }

  onInput(event: Event) {
    let txt = (<HTMLInputElement>event.target).value;
    this.text = txt;
  }
}
