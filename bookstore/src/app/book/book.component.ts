import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Book } from '../models/book.model';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Output() details = new EventEmitter();

  books = [
    new Book('title 1', 'description 1', './assets/book.webp', 50.50, '1000001'),
    new Book('title 2', 'description 2', './assets/book.webp', 50.50, '1000002'),
    new Book('title 3', 'description 3', './assets/book.webp', 50.50, '1000003'),
    new Book('title 4', 'description 4', './assets/book.webp', 50.50, '1000004'),
    new Book('title 5', 'description 5', './assets/book.webp', 50.50, '1000004'),
  ];

  ngOnInit(): void {
    this.details.emit(this.books[0]);
  }

  showDetail(title:string) {
    let abook:Book = new Book('', '', '', 0, '');
    for(let i=0; i<this.books.length; i++) {
      let book = this.books[i];
      if (book.title === title) {
        abook = book;
      }
    }
    this.details.emit(abook);
  }
}
