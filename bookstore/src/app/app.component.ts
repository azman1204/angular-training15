import { Component } from '@angular/core';
import { Book } from './models/book.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bookstore';
  abook:Book = new Book('', '', '', 0, '');
  menu = 'home';

  onDetails(book:Book) {
    //alert(book.title);
    this.abook = book;
  }

  showContent(menu:string) {
    this.menu = menu;
  }
}
