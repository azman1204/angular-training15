import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
// import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MenubarComponent } from './menubar/menubar.component';
import { BookComponent } from './book/book.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { GamesModule } from './games/games.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenubarComponent,
    BookComponent,
    BookDetailsComponent,
    ContactComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule, GamesModule, //FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
