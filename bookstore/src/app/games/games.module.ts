import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuessComponent } from './guess/guess.component';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    GuessComponent
  ],
  imports: [
    CommonModule, FormsModule
  ],
  exports: [
    GuessComponent
  ]
})
export class GamesModule { }
