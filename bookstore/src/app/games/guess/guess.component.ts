import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-guess',
  templateUrl: './guess.component.html',
  styleUrls: ['./guess.component.css']
})
export class GuessComponent implements OnInit {
  guessNo = 0;
  attempt = 0;
  randomNo = 0;
  message = '';
  win = false;

  ngOnInit(): void {
    let no = Math.random() * 100;
    let no2 = Math.round(no);
    console.log('no = ' + no, no2);
    this.randomNo = no2;
  }

  checkAnswer() {
    this.attempt++;

    if (this.guessNo == this.randomNo) {
      this.message = 'You win after ' + this.attempt;
    } else {
      this.message = 'Wrong ...#' + this.attempt;
      if (this.guessNo > this.randomNo) {
        this.message += " try lower";
      } else {
        this.message += " try higher";
      }
      this.win = true;
    }
  }

  playAgain() {
    let no = Math.random() * 100;
    let no2 = Math.round(no);
    this.randomNo = no2;
    this.attempt = 0;
    this.message = '';
    this.guessNo = 0;
    this.win =false;
  }
}
