function test() {
    console.log('Hello');
}
function sum(num1, num2) {
    return num1 + num2;
}
var tot = sum(5, 2);
console.log("5 + 2 = " + tot);
// type inference
var brand = 'Chevrolet'; // ts conclude this is string. no need type declaration here
// function and arrow function
var sayHello = function (name) {
    return 'Hello ' + name;
};
console.log(sayHello('John Doe'));
// Arrow Function
var sayHi = function (name) { return 'Hi ' + name; };
console.log(sayHi('Jane Doe'));
var sayHi2 = function (name) { return 'Hi ' + name; };
console.log(sayHi2('Jane Doe 2'));
// valid if there is only one parameter
var sayHi3 = function (myname) { return 'Hi ' + myname; };
console.log(sayHi3('Jane Doe 3'));
