"use strict";
exports.__esModule = true;
exports.Car = void 0;
var Car = /** @class */ (function () {
    function Car() {
        this.driveKm = 0;
    }
    Car.prototype.drive = function (km) {
        this.driveKm += km;
    };
    return Car;
}());
exports.Car = Car;
car: Car;
var car = new Car();
car.drive(50);
console.log('total drive = ' + car.driveKm);
