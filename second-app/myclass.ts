class Car {
    // access modifier - public / protected 
    // private (accessible only within this class)
    private distanceRun: number = 0;
    private color: string;
    made = 'Japan';
    //private isHybrid: boolean; // this is automatic injected here if constructor declare as private

    // a method automatically run when we create an object
    constructor(private isHybrid: boolean, color: string = 'red') {
        console.log('im in constructor');
        this.color = color;
        //this.isHybrid = isHybrid; // this is auto also
    }

    getGasConsumption() : string {
        return this.isHybrid ? 'very low' : 'Too High'; // tertiary operator
        // if (this.isHybrid) {
        //     return 'very low';
        // } else {
        //     return 'too high';
        // }
    }

    drive(distance: number): void {
        this.distanceRun += distance; // this.distanceRun = this.dsitanceRun + distance
    }

    // property accessor
    get distance(): number {
        return this.distanceRun;
    }

    // static method
    static honk(): string {
        return "HOOONNNKKK";
    }
}

let car1: Car = new Car(true);
car1.made = 'France';
car1.drive(200);
car1.drive(50);
console.log("total sitance = " + car1.distance); // we call method like a property

let car2: Car = new Car(true, 'green');
console.log('Gas consumption is ' + car2.getGasConsumption());

// calling static method. static method belongs to class, not to obj
console.log(Car.honk());