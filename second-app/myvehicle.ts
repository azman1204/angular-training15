interface Vehicle {
    make: string;
    drive(km:number):void;
}

export class Car implements Vehicle {
    driveKm = 0;
    make:string;

    constructor() {}

    drive(km:number) {
        this.driveKm += km;
    }
}

car: Car;
let car = new Car();
car.drive(50);
console.log('total drive = ' + car.driveKm);