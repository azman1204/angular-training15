var Car = /** @class */ (function () {
    //private isHybrid: boolean; // this is automatic injected here if constructor declare as private
    // a method automatically run when we create an object
    function Car(isHybrid, color) {
        if (color === void 0) { color = 'red'; }
        this.isHybrid = isHybrid;
        // access modifier - public / protected 
        // private (accessible only within this class)
        this.distanceRun = 0;
        this.made = 'Japan';
        console.log('im in constructor');
        this.color = color;
        //this.isHybrid = isHybrid; // this is auto also
    }
    Car.prototype.getGasConsumption = function () {
        return this.isHybrid ? 'very low' : 'Too High'; // tertiary operator
        // if (this.isHybrid) {
        //     return 'very low';
        // } else {
        //     return 'too high';
        // }
    };
    Car.prototype.drive = function (distance) {
        this.distanceRun += distance; // this.distanceRun = this.dsitanceRun + distance
    };
    Object.defineProperty(Car.prototype, "distance", {
        // property accessor
        get: function () {
            return this.distanceRun;
        },
        enumerable: false,
        configurable: true
    });
    // static method
    Car.honk = function () {
        return "HOOONNNKKK";
    };
    return Car;
}());
var car1 = new Car(true);
car1.made = 'France';
car1.drive(200);
car1.drive(50);
console.log("total sitance = " + car1.distance); // we call method like a property
var car2 = new Car(true, 'green');
console.log('Gas consumption is ' + car2.getGasConsumption());
// calling static method. static method belongs to class, not to obj
console.log(Car.honk());
