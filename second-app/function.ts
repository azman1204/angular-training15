function test() : void {
    console.log('Hello');
}

function sum(num1:number, num2: number) : number {
    return num1 + num2;
}

let tot = sum(5, 2);
console.log("5 + 2 = " + tot);

// type inference
const brand = 'Chevrolet'; // ts conclude this is string. no need type declaration here

// function and arrow function
const sayHello = function(name: string) : string {
    return 'Hello ' + name;
}

console.log(sayHello('John Doe'));

// Arrow Function
const sayHi = (name:string) => 'Hi ' + name;
console.log(sayHi('Jane Doe'));

const sayHi2 = (name:string) => { return 'Hi ' + name };
console.log(sayHi2('Jane Doe 2'));

// valid if there is only one parameter
const sayHi3 = myname => 'Hi ' + myname;
console.log(sayHi3('Jane Doe 3'));